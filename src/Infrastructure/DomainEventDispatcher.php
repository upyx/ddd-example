<?php

declare(strict_types=1);

namespace App\Infrastructure;

use App\Domain\Event\EventEmitterInterface;
use App\Domain\Event\EventInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * @codeCoverageIgnore
 */
final class DomainEventDispatcher extends EventDispatcher implements EventEmitterInterface
{
    /**
     * {@inheritdoc}
     */
    public function emit(EventInterface $event): void
    {
        $this->dispatch(\get_class($event), new DomainEvent($event));
    }
}
