<?php

declare(strict_types=1);

namespace App\Infrastructure;

abstract class AbstractRepository
{
    protected const FILE_NAME = '';

    /**
     * @var string
     */
    private $dataDir;

    /**
     * @var object|null
     */
    private $lastModel = null;

    /**
     * OutsideRepository constructor.
     *
     * @param string $dataDir
     */
    public function __construct(string $dataDir)
    {
        $this->dataDir = $dataDir;
    }

    public function saveLast(): void
    {
        $this->saveModel(static::FILE_NAME, $this->lastModel);
    }

    abstract protected function serializeModel(object $model): string;

    abstract protected function unserializeModel(string $data): object;

    protected function loadModel(string $file): object
    {
        if (null === $this->lastModel) {
            $this->ensureDataDirExists();
            $file = $this->dataDir . '/' . $file;

            $data = is_readable($file) ? file_get_contents($file) : '';

            $this->lastModel = $this->unserializeModel($data);
        }

        return $this->lastModel;
    }

    protected function saveModel(string $file, object $model): void
    {
        $this->lastModel = $model;

        $this->ensureDataDirExists();
        $file = $this->dataDir . '/' . $file;

        $data = $this->serializeModel($model);

        file_put_contents($file, $data);
    }

    private function ensureDataDirExists(): void
    {
        if (is_readable($this->dataDir) && is_dir($this->dataDir)) {
            return;
        }

        mkdir($this->dataDir, 0777, true);
    }
}
