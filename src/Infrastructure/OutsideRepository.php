<?php

declare(strict_types=1);

namespace App\Infrastructure;

use App\Domain\Model\Outside\Outside;
use App\Domain\Model\Outside\OutsideRepositoryInterface;
use App\Domain\Model\Outside\TimeOfDay;
use App\Domain\Model\Outside\Weather;

final class OutsideRepository extends AbstractRepository implements OutsideRepositoryInterface
{
    protected const FILE_NAME = 'Outside.json';

    public function loadOutside(): Outside
    {
        /* @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->loadModel(self::FILE_NAME);
    }

    public function saveOutside(Outside $outside): void
    {
        $this->saveModel(self::FILE_NAME, $outside);
    }

    protected function unserializeModel(string $data): object
    {
        $data = $data ? json_decode($data, true) : [];

        $weather = $data['weather'] ?? 'sunny';
        $timeOfDay = $data['timeOfDay'] ?? 'morning';

        return new Outside(Weather::{$weather}(), TimeOfDay::{$timeOfDay}());
    }

    protected function serializeModel(object $model): string
    {
        return json_encode([
            'weather' => (string) $model->getWeather(),
            'timeOfDay' => (string) $model->getTimeOfDay(),
        ]);
    }
}
