<?php

declare(strict_types=1);

namespace App\Infrastructure;

use App\Domain\Event\EventInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * @codeCoverageIgnore
 */
final class DomainEvent extends Event
{
    /**
     * @var EventInterface
     */
    private $domainEvent;

    /**
     * DomainEvent constructor.
     *
     * @param EventInterface $domainEvent
     */
    public function __construct(EventInterface $domainEvent)
    {
        $this->domainEvent = $domainEvent;
    }

    public function getDomainEvent(): EventInterface
    {
        return $this->domainEvent;
    }
}
