<?php

declare(strict_types=1);

namespace App\Infrastructure;

use App\Domain\Model\Window\Window;
use App\Domain\Model\Window\WindowRepositoryInterface;

final class WindowRepository extends AbstractRepository implements WindowRepositoryInterface
{
    protected const FILE_NAME = 'Window.json';

    public function loadWindow(): Window
    {
        /* @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->loadModel(self::FILE_NAME);
    }

    public function saveWindow(Window $window): void
    {
        $this->saveModel(self::FILE_NAME, $window);
    }

    protected function unserializeModel(string $data): object
    {
        $data = $data ? json_decode($data, true) : [];

        $state = $data['state'] ?? 'opened';
        $width = $data['width'] ?? 2;
        $height = $data['height'] ?? 2;

        $factory = ('closed' === $state) ? 'newClosed' : 'newOpened';

        return Window::{$factory}((int) $width, (int) $height);
    }

    protected function serializeModel(object $model): string
    {
        return json_encode([
            'state' => $model->isOpened() ? 'opened' : 'closed',
            'width' => $model->getWidth(),
            'height' => $model->getHeight(),
        ]);
    }
}
