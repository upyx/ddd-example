<?php

declare(strict_types=1);

namespace App\Domain\Model\Outside;

/**
 * Class Outside.
 *
 * @root
 */
final class Outside
{
    /**
     * @var Weather
     */
    private $weather;

    /**
     * @var TimeOfDay
     */
    private $timeOfDay;

    /**
     * Outside constructor.
     *
     * @param Weather   $weather
     * @param TimeOfDay $timeOfDay
     */
    public function __construct(Weather $weather, TimeOfDay $timeOfDay)
    {
        $this->weather = $weather;
        $this->timeOfDay = $timeOfDay;
    }

    /**
     * @return Weather
     */
    public function getWeather(): Weather
    {
        return $this->weather;
    }

    /**
     * @param Weather $weather
     */
    public function changeWeather(Weather $weather): void
    {
        $this->weather = $weather;
    }

    /**
     * @return TimeOfDay
     */
    public function getTimeOfDay(): TimeOfDay
    {
        return $this->timeOfDay;
    }

    /**
     * @param TimeOfDay $timeOfDay
     */
    public function changeTimeOfDay(TimeOfDay $timeOfDay): void
    {
        $this->timeOfDay = $timeOfDay;
    }
}
