<?php

declare(strict_types=1);

namespace App\Domain\Model\Outside;

/**
 * Class TimeOfDay.
 *
 * @value
 * @enum
 */
final class TimeOfDay
{
    /**
     * @var string
     */
    private $state;

    /**
     * Закрытый конструктор для внутренненего использования.
     */
    private function __construct(string $state)
    {
        $this->state = $state;
    }

    /**
     * Конструктор для утра.
     */
    public static function morning(): self
    {
        return new self(__FUNCTION__);
    }

    /**
     * Конструктор для дня.
     */
    public static function afternoon(): self
    {
        return new self(__FUNCTION__);
    }

    /**
     * Конструктор для вечера.
     */
    public static function evening(): self
    {
        return new self(__FUNCTION__);
    }

    /**
     * Конструктор для ночи.
     */
    public static function night(): self
    {
        return new self(__FUNCTION__);
    }

    /**
     * Сравнивает время суток на идентичность.
     */
    public function equals(self $timeOfDay): bool
    {
        return $this->state === $timeOfDay->state;
    }

    public function __toString(): string
    {
        return $this->state;
    }
}
