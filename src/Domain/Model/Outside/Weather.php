<?php

declare(strict_types=1);

namespace App\Domain\Model\Outside;

/**
 * Class Weather.
 *
 * @value
 * @enum
 */
final class Weather
{
    private const STATE_SUNNY = 'sunny';
    private const STATE_RAINY = 'rainy';

    /**
     * @var string
     */
    private $state;

    /**
     * Закрытый конструктор погоды для внутреннего использования.
     */
    private function __construct(string $state)
    {
        $this->state = $state;
    }

    /**
     * Конструктор для солнечной погоды.
     */
    public static function sunny(): self
    {
        return new self(self::STATE_SUNNY);
    }

    /**
     * Конструктор для дождливой погоды.
     */
    public static function rainy(): self
    {
        return new self(self::STATE_RAINY);
    }

    /**
     * Сравнивает погоду.
     */
    public function equals(self $weather): bool
    {
        return $this->state === $weather->state;
    }

    public function __toString(): string
    {
        return $this->state;
    }
}
