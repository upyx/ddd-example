<?php

declare(strict_types=1);

namespace App\Domain\Model\Outside;

interface OutsideRepositoryInterface
{
    /**
     * @return Outside
     */
    public function loadOutside(): Outside;

    /**
     * @param Outside $outside
     */
    public function saveOutside(Outside $outside): void;
}
