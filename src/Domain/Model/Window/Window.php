<?php

declare(strict_types=1);

namespace App\Domain\Model\Window;

/**
 * Class Window.
 *
 * @root
 */
final class Window
{
    private const STATE_OPENED = 0;
    private const STATE_CLOSED = 1;

    /**
     * @var WindowSize
     */
    private $size;

    /**
     * @var int
     */
    private $state;

    /**
     * Закрытый конструктор окон для внутреннего использования.
     *
     * @param WindowSize $size
     * @param int        $state
     */
    private function __construct(WindowSize $size, int $state)
    {
        $this->size = $size;
        $this->state = $state;
    }

    /**
     * Конструктор для открытого окна.
     */
    public static function newOpened(int $width, int $height): self
    {
        return new self(new WindowSize($width, $height), self::STATE_OPENED);
    }

    /**
     * Конструктор для закрытого окна.
     */
    public static function newClosed(int $width, int $height): self
    {
        return new self(new WindowSize($width, $height), self::STATE_CLOSED);
    }

    /**
     * Возвращает ширину окна.
     */
    public function getWidth(): int
    {
        return $this->size->getWidth();
    }

    /**
     * Возвращает высоту окна.
     */
    public function getHeight(): int
    {
        return $this->size->getHeight();
    }

    /**
     * Возвращает открыто ли окно.
     */
    public function isOpened(): bool
    {
        return self::STATE_OPENED === $this->state;
    }

    /**
     * Возвращает закрыто ли окно.
     */
    public function isClosed(): bool
    {
        return self::STATE_CLOSED === $this->state;
    }

    /**
     * Открывает окно.
     */
    public function open(): void
    {
        $this->state = self::STATE_OPENED;
    }

    /**
     * Закрывает окно.
     */
    public function close(): void
    {
        $this->state = self::STATE_CLOSED;
    }
}
