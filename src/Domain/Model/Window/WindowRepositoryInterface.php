<?php

declare(strict_types=1);

namespace App\Domain\Model\Window;

interface WindowRepositoryInterface
{
    /**
     * @return Window
     */
    public function loadWindow(): Window;

    /**
     * @param Window $window
     */
    public function saveWindow(Window $window): void;
}
