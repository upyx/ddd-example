<?php

declare(strict_types=1);

namespace App\Domain\Service;

use App\Domain\Event\EventEmitterInterface;
use App\Domain\Event\Window\WindowClosedEvent;
use App\Domain\Event\Window\WindowOpenedEvent;
use App\Domain\Model\Window\Window;

final class WindowStateService
{
    /**
     * @var EventEmitterInterface
     */
    private $eventEmitter;

    /**
     * WindowStateService constructor.
     *
     * @param EventEmitterInterface $eventEmitter
     */
    public function __construct(EventEmitterInterface $eventEmitter)
    {
        $this->eventEmitter = $eventEmitter;
    }

    /**
     * Открывает окно и выбрасывает событие об этом, если окно было закрыто.
     *
     * @param Window $window
     *
     * @return bool - false, если окно уже открыто
     */
    public function openIfClosed(Window $window): bool
    {
        if (!$window->isClosed()) {
            return false;
        }

        $window->open();
        $this->eventEmitter->emit(new WindowOpenedEvent($window));

        return true;
    }

    /**
     * Закрывает окно и выбрасывает событие об этом, если окно было открыто.
     *
     * @param Window $window
     *
     * @return bool - false, если окно уже закрыто
     */
    public function closeIfOpened(Window $window): bool
    {
        if (!$window->isOpened()) {
            return false;
        }

        $window->close();
        $this->eventEmitter->emit(new WindowClosedEvent($window));

        return true;
    }
}
