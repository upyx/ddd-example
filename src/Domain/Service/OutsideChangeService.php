<?php

declare(strict_types=1);

namespace App\Domain\Service;

use App\Domain\Event\EventEmitterInterface;
use App\Domain\Event\Outside\OutsideTimeOfDayChangedEvent;
use App\Domain\Event\Outside\OutsideWeatherChangedEvent;
use App\Domain\Model\Outside\Outside;
use App\Domain\Model\Outside\TimeOfDay;
use App\Domain\Model\Outside\Weather;

final class OutsideChangeService
{
    /**
     * @var EventEmitterInterface
     */
    private $eventEmitter;

    public function __construct(EventEmitterInterface $eventEmitter)
    {
        $this->eventEmitter = $eventEmitter;
    }

    /**
     * Изменяет погоду на указанную, если установлена другая и выбрасывает об этом событие.
     *
     * @param Outside $outside
     * @param Weather $weather
     *
     * @return bool - false, если такая погода уже установлена
     */
    public function changeWeatherToIfNeeded(Outside $outside, Weather $weather): bool
    {
        $oldWeather = $outside->getWeather();
        if ($oldWeather->equals($weather)) {
            return false;
        }

        $outside->changeWeather($weather);
        $this->eventEmitter->emit(new OutsideWeatherChangedEvent($outside));

        return true;
    }

    /**
     * Изменяет время суток на указанное, если установлено другое и выбрасывает об этом событие.
     *
     * @param Outside   $outside
     * @param TimeOfDay $timeOfDay
     *
     * @return bool - false, если такая погода уже установлена
     */
    public function changeTimeOdDayToIfNeeded(Outside $outside, TimeOfDay $timeOfDay): bool
    {
        $oldTimeOfDay = $outside->getTimeOfDay();
        if ($oldTimeOfDay->equals($timeOfDay)) {
            return false;
        }

        $outside->changeTimeOfDay($timeOfDay);
        $this->eventEmitter->emit(new OutsideTimeOfDayChangedEvent($outside));

        return true;
    }
}
