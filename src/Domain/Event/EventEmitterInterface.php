<?php

declare(strict_types=1);

namespace App\Domain\Event;

interface EventEmitterInterface
{
    /**
     * Выбрасывает собития для внешних систем
     *
     * @param EventInterface $event
     */
    public function emit(EventInterface $event): void;
}
