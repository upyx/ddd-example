<?php

declare(strict_types=1);

namespace App\Domain\Event\Outside;

final class OutsideWeatherChangedEvent extends AbstractOutsideEvent
{
}
