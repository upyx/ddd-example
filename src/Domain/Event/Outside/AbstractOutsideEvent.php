<?php

declare(strict_types=1);

namespace App\Domain\Event\Outside;

use App\Domain\Event\EventInterface;
use App\Domain\Model\Outside\Outside;

abstract class AbstractOutsideEvent implements EventInterface
{
    /**
     * @var Outside
     */
    private $outside;

    public function __construct(Outside $outside)
    {
        $this->outside = $outside;
    }

    public function getOutside(): Outside
    {
        return $this->outside;
    }
}
