<?php

declare(strict_types=1);

namespace App\Domain\Event\Outside;

final class OutsideTimeOfDayChangedEvent extends AbstractOutsideEvent
{
}
