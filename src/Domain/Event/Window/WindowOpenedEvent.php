<?php

declare(strict_types=1);

namespace App\Domain\Event\Window;

final class WindowOpenedEvent extends AbstractWindowEvent
{
}
