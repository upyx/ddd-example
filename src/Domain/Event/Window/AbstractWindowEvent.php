<?php

declare(strict_types=1);

namespace App\Domain\Event\Window;

use App\Domain\Event\EventInterface;
use App\Domain\Model\Window\Window;

abstract class AbstractWindowEvent implements EventInterface
{
    /**
     * @var Window
     */
    private $window;

    public function __construct(Window $window)
    {
        $this->window = $window;
    }

    public function getWindow(): Window
    {
        return $this->window;
    }
}
