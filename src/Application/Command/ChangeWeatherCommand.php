<?php

declare(strict_types=1);

namespace App\Application\Command;

use App\Application\Listener\OutsideChangedListener;
use App\Domain\Event\Window\WindowClosedEvent;
use App\Domain\Event\Window\WindowOpenedEvent;
use App\Domain\Model\Outside\Weather;
use App\Domain\Service\OutsideChangeService;
use App\Domain\Service\WindowStateService;
use App\Infrastructure\DomainEventDispatcher;
use App\Infrastructure\OutsideRepository;
use App\Infrastructure\WindowRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

final class ChangeWeatherCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected static $defaultName = 'change-weather';

    /**
     * @var WindowStateService
     */
    private $windowStateService;

    /**
     * @var OutsideChangeService
     */
    private $outsideChangeService;

    /**
     * @var OutsideRepository
     */
    private $outsideRepository;

    /**
     * @var WindowRepository
     */
    private $windowRepository;

    /**
     * @var string[]
     */
    private $messages = [];

    public function __construct()
    {
        parent::__construct();

        $dataDir = \dirname(\dirname(\dirname(__DIR__))) . '/var';
        $this->outsideRepository = new OutsideRepository($dataDir);
        $this->windowRepository = new WindowRepository($dataDir);

        $eventEmitter = new DomainEventDispatcher();
        $this->windowStateService = new WindowStateService($eventEmitter);
        $this->outsideChangeService = new OutsideChangeService($eventEmitter);

        $outsideChangedListener = new OutsideChangedListener($this->windowRepository, $this->windowStateService);

        $eventEmitter->addSubscriber($outsideChangedListener);
        $eventEmitter->addListener(WindowOpenedEvent::class, function (): void {
            $this->messages[] = 'The window was opened.';
        });
        $eventEmitter->addListener(WindowClosedEvent::class, function (): void {
            $this->messages[] = 'The window was closed.';
        });
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Make sunny weather')
            ->addArgument('weather', InputArgument::REQUIRED, 'Schema name');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $io = new SymfonyStyle($input, $output);
        $weather = $input->getArgument('weather');

        $validWeather = ['sunny', 'rainy'];
        if (!\in_array($weather, $validWeather, true)) {
            $io->error(sprintf('Unknown weather %s. You cat use %s.', $weather, implode(' or ', $validWeather)));

            return;
        }

        $this->windowRepository->loadWindow();
        $outside = $this->outsideRepository->loadOutside();
        $this->outsideChangeService->changeWeatherToIfNeeded($outside, Weather::{$weather}());

        foreach ($this->messages as $message) {
            $io->note($message);
        }

        $this->windowRepository->saveLast();
        $this->outsideRepository->saveLast();

        $io->success(sprintf('It is %s.', $weather));
    }
}
