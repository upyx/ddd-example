<?php

declare(strict_types=1);

namespace App\Application\Listener;

use App\Domain\Event\Outside\AbstractOutsideEvent;
use App\Domain\Event\Outside\OutsideTimeOfDayChangedEvent;
use App\Domain\Event\Outside\OutsideWeatherChangedEvent;
use App\Domain\Model\Outside\TimeOfDay;
use App\Domain\Model\Outside\Weather;
use App\Domain\Model\Window\Window;
use App\Domain\Model\Window\WindowRepositoryInterface;
use App\Domain\Service\WindowStateService;
use App\Infrastructure\DomainEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class OutsideChangedListener implements EventSubscriberInterface
{
    /**
     * @var WindowRepositoryInterface
     */
    private $windowRepository;

    /**
     * @var WindowStateService
     */
    private $windowStateService;

    /**
     * OutsideChangedListener constructor.
     *
     * @param WindowRepositoryInterface $windowRepository
     * @param WindowStateService        $windowStateService
     */
    public function __construct(WindowRepositoryInterface $windowRepository, WindowStateService $windowStateService)
    {
        $this->windowRepository = $windowRepository;
        $this->windowStateService = $windowStateService;
    }

    public function handleEvent(DomainEvent $event): void
    {
        /** @var AbstractOutsideEvent $outsideEvent */
        $outsideEvent = $event->getDomainEvent();

        $timeOfDay = $outsideEvent->getOutside()->getTimeOfDay();
        $weather = $outsideEvent->getOutside()->getWeather();
        $window = $this->windowRepository->loadWindow();

        if (Weather::rainy()->equals($weather)) {
            $this->closeWindow($window);

            return;
        }

        if (TimeOfDay::night()->equals($timeOfDay)) {
            if ($window->getWidth() >= 2 && $window->getHeight() >= 2) {
                $this->closeWindow($window);

                return;
            }
        } else {
            if ($window->getWidth() < 2 && $window->getHeight() < 2) {
                $this->openWindow($window);

                return;
            }
        }
    }

    private function openWindow(Window $window): void
    {
        $this->windowStateService->openIfClosed($window);
    }

    private function closeWindow(Window $window): void
    {
        $this->windowStateService->closeIfOpened($window);
    }

    /**
     * @codeCoverageIgnore
     *
     * @return string[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            OutsideTimeOfDayChangedEvent::class => 'handleEvent',
            OutsideWeatherChangedEvent::class => 'handleEvent',
        ];
    }
}
