<?php

declare(strict_types=1);

namespace App\Tests\Domain\Model\Outside;

use App\Domain\Model\Outside\Outside;
use App\Domain\Model\Outside\TimeOfDay;
use App\Domain\Model\Outside\Weather;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Domain\Model\Outside\Outside
 */
final class OutsideTest extends TestCase
{
    public function testWeather(): void
    {
        $sunny = Weather::sunny();
        $rainy = Weather::rainy();

        $outside = new Outside($sunny, TimeOfDay::afternoon());

        $initialWeather = $outside->getWeather();
        $outside->changeWeather($rainy);
        $actualWeather = $outside->getWeather();

        $this->assertSame($sunny, $initialWeather);
        $this->assertSame($rainy, $actualWeather);
    }

    public function testTimeOfDay(): void
    {
        $morning = TimeOfDay::morning();
        $evening = TimeOfDay::evening();

        $outside = new Outside(Weather::sunny(), $morning);

        $initialTimeOfDay = $outside->getTimeOfDay();
        $outside->changeTimeOfDay($evening);
        $actualTimeOfDay = $outside->getTimeOfDay();

        $this->assertSame($morning, $initialTimeOfDay);
        $this->assertSame($evening, $actualTimeOfDay);
    }
}
