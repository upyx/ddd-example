<?php

declare(strict_types=1);

namespace App\Tests\Domain\Model\Outside;

use App\Domain\Model\Outside\TimeOfDay;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Domain\Model\Outside\TimeOfDay
 */
final class TimeOfDayTest extends TestCase
{
    public function providerVariants(): iterable
    {
        $variants = [
            'morning',
            'afternoon',
            'evening',
            'night',
        ];

        foreach ($variants as $outer) {
            foreach ($variants as $variant) {
                yield sprintf('%s_%s', $outer, $variant) => [$outer, $variant, $outer === $variant];
            }
        }
    }

    /**
     * @dataProvider providerVariants
     */
    public function testVariants(string $first, string $second, bool $equals): void
    {
        $first = TimeOfDay::{$first}();
        $second = TimeOfDay::{$second}();

        /* @noinspection PhpUndefinedMethodInspection */
        $this->assertSame($equals, $first->equals($second));
    }

    public function testToString(): void
    {
        $variants = [
            'morning',
            'afternoon',
            'evening',
            'night',
        ];

        foreach ($variants as $variant) {
            $this->assertSame($variant, (string) TimeOfDay::{$variant}());
        }
    }
}
