<?php

declare(strict_types=1);

namespace App\Tests\Domain\Model\Outside;

use App\Domain\Model\Outside\Weather;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Domain\Model\Outside\Weather
 */
final class WeatherTest extends TestCase
{
    public function testEquals(): void
    {
        $sunny = Weather::sunny();
        $rainy = Weather::rainy();

        $this->assertTrue($sunny->equals(clone $sunny));
        $this->assertTrue($rainy->equals(clone $rainy));
        $this->assertFalse($rainy->equals($sunny));
        $this->assertFalse($sunny->equals($rainy));
    }

    public function testToString(): void
    {
        $this->assertSame('sunny', (string) Weather::sunny());
        $this->assertSame('rainy', (string) Weather::rainy());
    }
}
