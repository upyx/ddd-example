<?php

declare(strict_types=1);

namespace App\Tests\Domain\Model\Window;

use App\Domain\Model\Window\Window;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Domain\Model\Window\Window
 * @covers \App\Domain\Model\Window\WindowSize
 */
final class WindowTest extends TestCase
{
    public function testOpenWhenClosed(): void
    {
        $w = Window::newClosed(2, 2);
        $w->open();

        $this->assertTrue($w->isOpened());
        $this->assertFalse($w->isClosed());
    }

    public function testOpenWhenOpened(): void
    {
        $w = Window::newOpened(2, 2);
        $w->open();

        $this->assertTrue($w->isOpened());
        $this->assertFalse($w->isClosed());
    }

    public function testCloseWhenCosed(): void
    {
        $w = Window::newClosed(2, 2);
        $w->close();

        $this->assertFalse($w->isOpened());
        $this->assertTrue($w->isClosed());
    }

    public function testCloseWhenOpened(): void
    {
        $w = Window::newOpened(2, 2);
        $w->close();

        $this->assertFalse($w->isOpened());
        $this->assertTrue($w->isClosed());
    }

    public function testGetWidthAndHeight(): void
    {
        $w = Window::newOpened(3, 5);
        $width = $w->getWidth();
        $height = $w->getHeight();

        $this->assertSame(3, $width);
        $this->assertSame(5, $height);
    }

    public function testIsClosed(): void
    {
        $wOpened = Window::newOpened(2, 2);
        $wClosed = Window::newClosed(2, 2);

        $this->assertFalse($wOpened->isClosed());
        $this->assertTrue($wClosed->isClosed());
    }

    public function testIsOpened(): void
    {
        $wOpened = Window::newOpened(2, 2);
        $wClosed = Window::newClosed(2, 2);

        $this->assertTrue($wOpened->isOpened());
        $this->assertFalse($wClosed->isOpened());
    }
}
