<?php

declare(strict_types=1);

namespace App\Tests\Domain\Event\Outside;

use App\Domain\Event\Outside\AbstractOutsideEvent;
use App\Domain\Model\Outside\Outside;
use App\Domain\Model\Outside\TimeOfDay;
use App\Domain\Model\Outside\Weather;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Domain\Event\Outside\AbstractOutsideEvent
 */
final class AbstractOutsideEventTest extends TestCase
{
    public function testGetOutside(): void
    {
        $outside = new Outside(Weather::sunny(), TimeOfDay::evening());

        /** @var AbstractOutsideEvent $outsideEvent */
        $outsideEvent = $this
            ->getMockBuilder(AbstractOutsideEvent::class)
            ->setConstructorArgs([$outside])
            ->getMockForAbstractClass();

        $this->assertSame($outside, $outsideEvent->getOutside());
    }
}
