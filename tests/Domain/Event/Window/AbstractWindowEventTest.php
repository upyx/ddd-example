<?php

declare(strict_types=1);

namespace App\Tests\Domain\Event\Window;

use App\Domain\Event\Window\AbstractWindowEvent;
use App\Domain\Model\Window\Window;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Domain\Event\Window\AbstractWindowEvent
 */
final class AbstractWindowEventTest extends TestCase
{
    public function testGetWindow(): void
    {
        $window = Window::newOpened(3, 4);

        /** @var AbstractWindowEvent $windowEvent */
        $windowEvent = $this
            ->getMockBuilder(AbstractWindowEvent::class)
            ->setConstructorArgs([$window])
            ->getMockForAbstractClass();

        $this->assertSame($window, $windowEvent->getWindow());
    }
}
