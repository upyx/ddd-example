<?php

declare(strict_types=1);

namespace App\Tests\Domain\Service;

use App\Domain\Event\EventEmitterInterface;
use App\Domain\Event\Window\WindowClosedEvent;
use App\Domain\Event\Window\WindowOpenedEvent;
use App\Domain\Model\Window\Window;
use App\Domain\Service\WindowStateService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Domain\Service\WindowStateService
 */
final class WindowStateServiceTest extends TestCase
{
    /**
     * @var WindowStateService
     */
    private $windowStateService;

    /**
     * @var EventEmitterInterface|MockObject
     */
    private $eventEmitter;

    protected function setUp(): void
    {
        parent::setUp();

        /** @var EventEmitterInterface|MockObject $eventEmitter */
        $eventEmitter = $this
            ->getMockBuilder(EventEmitterInterface::class)
            ->getMockForAbstractClass();

        $this->windowStateService = new WindowStateService($eventEmitter);
        $this->eventEmitter = $eventEmitter;
    }

    public function testOpenWhenClosed(): void
    {
        /** @var WindowOpenedEvent $event */
        $event = null;
        $this
            ->eventEmitter
            ->expects($this->once())
            ->method('emit')
            ->willReturnCallback(function (WindowOpenedEvent $windowEvent) use (&$event): void {
                $event = $windowEvent;
            });

        $window = Window::newClosed(2, 2);

        $stateChanged = $this->windowStateService->openIfClosed($window);

        $this->assertTrue($stateChanged);
        $this->assertTrue($window->isOpened());
        $this->assertSame($window, $event->getWindow());
    }

    public function testCloseWhenOpened(): void
    {
        /** @var WindowClosedEvent $event */
        $event = null;
        $this
            ->eventEmitter
            ->expects($this->once())
            ->method('emit')
            ->willReturnCallback(function (WindowClosedEvent $windowEvent) use (&$event): void {
                $event = $windowEvent;
            });

        $window = Window::newOpened(2, 2);

        // test mock
        $stateChanged = $this->windowStateService->closeIfOpened($window);

        $this->assertTrue($stateChanged);
        $this->assertTrue($window->isClosed());
        $this->assertSame($window, $event->getWindow());
    }

    public function testOpenWhenOpened(): void
    {
        $this
            ->eventEmitter
            ->expects($this->never())
            ->method('emit');

        $window = Window::newOpened(2, 2);

        // test mock
        $stateChanged = $this->windowStateService->openIfClosed($window);

        $this->assertFalse($stateChanged);
        $this->assertTrue($window->isOpened());
    }

    public function testCloseWhenClosed(): void
    {
        $this
            ->eventEmitter
            ->expects($this->never())
            ->method('emit');

        $window = Window::newClosed(2, 2);

        // test mock
        $stateChanged = $this->windowStateService->closeIfOpened($window);

        $this->assertFalse($stateChanged);
        $this->assertTrue($window->isClosed());
    }
}
