<?php

declare(strict_types=1);

namespace App\Tests\Domain\Service;

use App\Domain\Event\EventEmitterInterface;
use App\Domain\Event\Outside\OutsideTimeOfDayChangedEvent;
use App\Domain\Event\Outside\OutsideWeatherChangedEvent;
use App\Domain\Model\Outside\Outside;
use App\Domain\Model\Outside\TimeOfDay;
use App\Domain\Model\Outside\Weather;
use App\Domain\Service\OutsideChangeService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Domain\Service\OutsideChangeService
 */
final class OutsideChangeServiceTest extends TestCase
{
    /**
     * @var OutsideChangeService
     */
    private $outsideChangeService;

    /**
     * @var EventEmitterInterface|MockObject
     */
    private $eventEmitter;

    protected function setUp(): void
    {
        parent::setUp();

        /** @var EventEmitterInterface|MockObject $eventEmitter */
        $eventEmitter = $this
            ->getMockBuilder(EventEmitterInterface::class)
            ->getMockForAbstractClass();

        $this->outsideChangeService = new OutsideChangeService($eventEmitter);
        $this->eventEmitter = $eventEmitter;
    }

    public function testChangeWeatherToSunnyWhenSunny(): void
    {
        $this
            ->eventEmitter
            ->expects($this->never())
            ->method('emit');

        $outside = new Outside(Weather::sunny(), TimeOfDay::afternoon());

        $stateChanged = $this->outsideChangeService->changeWeatherToIfNeeded($outside, Weather::sunny());

        $this->assertFalse($stateChanged);
        $this->assertTrue($outside->getWeather()->equals(Weather::sunny()));
    }

    public function testChangeWeatherToRainyWhenSunny(): void
    {
        /** @var OutsideWeatherChangedEvent $event */
        $event = null;
        $this
            ->eventEmitter
            ->expects($this->once())
            ->method('emit')
            ->willReturnCallback(function (OutsideWeatherChangedEvent $outsideEvent) use (&$event): void {
                $event = $outsideEvent;
            });

        $outside = new Outside(Weather::sunny(), TimeOfDay::afternoon());

        $stateChanged = $this->outsideChangeService->changeWeatherToIfNeeded($outside, Weather::rainy());

        $this->assertTrue($stateChanged);
        $this->assertTrue($outside->getWeather()->equals(Weather::rainy()));
        $this->assertSame($outside, $event->getOutside());
    }

    public function testChangeTimeOfDayToMorningWhenMorning(): void
    {
        $this
            ->eventEmitter
            ->expects($this->never())
            ->method('emit');

        $outside = new Outside(Weather::sunny(), TimeOfDay::morning());

        $stateChanged = $this->outsideChangeService->changeTimeOdDayToIfNeeded($outside, TimeOfDay::morning());

        $this->assertFalse($stateChanged);
        $this->assertTrue($outside->getTimeOfDay()->equals(TimeOfDay::morning()));
    }

    public function testChangeTimeOfDayToEveningWhenMorning(): void
    {
        /** @var OutsideTimeOfDayChangedEvent $event */
        $event = null;
        $this
            ->eventEmitter
            ->expects($this->once())
            ->method('emit')
            ->willReturnCallback(function (OutsideTimeOfDayChangedEvent $outsideEvent) use (&$event): void {
                $event = $outsideEvent;
            });

        $outside = new Outside(Weather::sunny(), TimeOfDay::morning());

        $stateChanged = $this->outsideChangeService->changeTimeOdDayToIfNeeded($outside, TimeOfDay::evening());

        $this->assertTrue($stateChanged);
        $this->assertTrue($outside->getTimeOfDay()->equals(TimeOfDay::evening()));
        $this->assertSame($outside, $event->getOutside());
    }
}
