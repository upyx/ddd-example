<?php

declare(strict_types=1);

namespace App\Tests\Application\Listener;

use App\Application\Listener\OutsideChangedListener;
use App\Domain\Event\EventEmitterInterface;
use App\Domain\Event\Outside\AbstractOutsideEvent;
use App\Domain\Model\Outside\Outside;
use App\Domain\Model\Outside\TimeOfDay;
use App\Domain\Model\Outside\Weather;
use App\Domain\Model\Window\Window;
use App\Domain\Model\Window\WindowRepositoryInterface;
use App\Domain\Service\WindowStateService;
use App\Infrastructure\DomainEvent;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Application\Listener\OutsideChangedListener
 */
final class OutsideChangedListenerTest extends TestCase
{
    public function providerCloseAnyWindowWhenRainy(): iterable
    {
        $windows = [
            'small' => Window::newOpened(1, 1),
            'medium' => Window::newOpened(2, 1),
            'big' => Window::newOpened(2, 2),
        ];

        $times = [
            'morning' => TimeOfDay::morning(),
            'afternoon' => TimeOfDay::afternoon(),
            'evening' => TimeOfDay::evening(),
            'night' => TimeOfDay::night(),
        ];

        foreach ($windows as $outer => $window) {
            foreach ($times as $inner => $time) {
                yield sprintf('%s_%s', $outer, $inner) => [$window, $time];
            }
        }
    }

    /**
     * @dataProvider providerCloseAnyWindowWhenRainy
     */
    public function testCloseAnyWindowWhenRainy(Window $window, TimeOfDay $timeOfDay): void
    {
        $outside = new Outside(Weather::rainy(), $timeOfDay);
        $listener = $this->makeListener($window);
        $listener->handleEvent(new DomainEvent($this->makeOutsideEvent($outside)));

        $this->assertTrue($window->isClosed());
    }

    public function providerOpenSmallWindowWhenSunny(): iterable
    {
        return [
            'morning' => [TimeOfDay::morning()],
            'afternoon' => [TimeOfDay::afternoon()],
            'evening' => [TimeOfDay::evening()],
        ];
    }

    /**
     * @dataProvider providerOpenSmallWindowWhenSunny
     */
    public function testOpenSmallWindowWhenSunny(TimeOfDay $timeOfDay): void
    {
        $window = Window::newClosed(1, 1);
        $outside = new Outside(Weather::sunny(), $timeOfDay);
        $listener = $this->makeListener($window);

        $listener->handleEvent(new DomainEvent($this->makeOutsideEvent($outside)));

        $this->assertTrue($window->isOpened());
    }

    public function testCloseBigWindowWhenDryAtNight(): void
    {
        $window = Window::newOpened(2, 2);
        $outside = new Outside(Weather::sunny(), TimeOfDay::night());
        $listener = $this->makeListener($window);

        $listener->handleEvent(new DomainEvent($this->makeOutsideEvent($outside)));

        $this->assertTrue($window->isClosed());
    }

    public function testDoNothing(): void
    {
        $window = Window::newOpened(2, 2);
        $outside = new Outside(Weather::sunny(), TimeOfDay::afternoon());
        $listener = $this->makeListener($window);

        $listener->handleEvent(new DomainEvent($this->makeOutsideEvent($outside)));

        $this->assertTrue($window->isOpened());
    }

    private function makeListener(Window $window): OutsideChangedListener
    {
        /** @var WindowRepositoryInterface|MockObject $windowRepository */
        $windowRepository = $this->createMock(WindowRepositoryInterface::class);
        $windowRepository->method('loadWindow')->willReturn($window);

        /** @var EventEmitterInterface $eventEmitter */
        $eventEmitter = $this->createMock(EventEmitterInterface::class);
        $windowStateService = new WindowStateService($eventEmitter);

        return new OutsideChangedListener($windowRepository, $windowStateService);
    }

    private function makeOutsideEvent(Outside $outside): AbstractOutsideEvent
    {
        /** @var AbstractOutsideEvent $outsideEvent */
        $outsideEvent = $this
            ->getMockBuilder(AbstractOutsideEvent::class)
            ->setConstructorArgs([$outside])
            ->getMockForAbstractClass();

        return $outsideEvent;
    }
}
