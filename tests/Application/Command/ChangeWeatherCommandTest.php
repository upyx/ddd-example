<?php

declare(strict_types=1);

namespace App\Tests\Application\Command;

use App\Application\Command\ChangeWeatherCommand;
use App\Domain\Model\Outside\Outside;
use App\Domain\Model\Outside\TimeOfDay;
use App\Domain\Model\Outside\Weather;
use App\Domain\Model\Window\Window;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * @covers \App\Application\Command\ChangeWeatherCommand
 */
final class ChangeWeatherCommandTest extends TestCase
{
    public function testExecuteRainyOnSunny(): void
    {
        $application = new Application();
        $application->add(new ChangeWeatherCommand());

        $command = $application->find('change-weather');
        $outsideRepository = $this->getObjectAttribute($command, 'outsideRepository');
        $windowRepository = $this->getObjectAttribute($command, 'windowRepository');

        $outsideRepository->saveOutside(new Outside(Weather::sunny(), TimeOfDay::afternoon()));
        $windowRepository->saveWindow(Window::newOpened(1, 1));

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command' => $command->getName(),
            'weather' => 'rainy',
        ]);

        $output = $commandTester->getDisplay();

        $this->assertContains('The window was closed', $output);
        $this->assertContains('It is rainy', $output);
    }

    public function testExecuteSunnyOnRainy(): void
    {
        $application = new Application();
        $application->add(new ChangeWeatherCommand());

        $command = $application->find('change-weather');
        $outsideRepository = $this->getObjectAttribute($command, 'outsideRepository');
        $windowRepository = $this->getObjectAttribute($command, 'windowRepository');

        $outsideRepository->saveOutside(new Outside(Weather::rainy(), TimeOfDay::afternoon()));
        $windowRepository->saveWindow(Window::newClosed(1, 1));

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command' => $command->getName(),
            'weather' => 'sunny',
        ]);

        $output = $commandTester->getDisplay();

        $this->assertContains('The window was opened', $output);
        $this->assertContains('It is sunny', $output);
    }

    public function testExecuteOnInvalidWeather(): void
    {
        $application = new Application();
        $application->add(new ChangeWeatherCommand());

        $command = $application->find('change-weather');

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command' => $command->getName(),
            'weather' => 'wrong',
        ]);

        $output = $commandTester->getDisplay();

        $this->assertContains('Unknown weather wrong. You cat use sunny or rainy.', $output);
    }
}
