<?php

declare(strict_types=1);

namespace App\Tests\Infrastructure;

use App\Domain\Model\Outside\Outside;
use App\Domain\Model\Outside\TimeOfDay;
use App\Domain\Model\Outside\Weather;
use App\Infrastructure\OutsideRepository;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Infrastructure\OutsideRepository
 */
final class OutsideRepositoryTest extends TestCase
{
    use TemporaryDirectory;

    /**
     * @var string
     */
    private static $json = '{"weather":"sunny","timeOfDay":"afternoon"}';

    /**
     * @var string
     */
    private static $fileName = 'Outside.json';

    /**
     * @var Outside
     */
    private static $outside;

    /**
     * @var string
     */
    private $tempDir;

    /**
     * @var OutsideRepository
     */
    private $repository;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::$outside = new Outside(Weather::sunny(), TimeOfDay::afternoon());
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->tempDir = $this->prepareTmpDir();
        $this->repository = new OutsideRepository($this->tempDir);
    }

    protected function tearDown(): void
    {
        $this->eraseTmpDir($this->tempDir);

        parent::tearDown();
    }

    public function testLoadOutside(): void
    {
        file_put_contents($this->getFilePath(), self::$json);

        $outside = $this->repository->loadOutside();

        $this->assertEquals(self::$outside, $outside);
    }

    public function testSaveOutside(): void
    {
        $this->repository->saveOutside(self::$outside);
        $json = file_get_contents($this->getFilePath());

        $this->assertSame(self::$json, $json);
    }

    private function getFilePath(): string
    {
        return $this->tempDir . DIRECTORY_SEPARATOR . self::$fileName;
    }
}
