<?php

declare(strict_types=1);

namespace App\Tests\Infrastructure;

trait TemporaryDirectory
{
    protected function prepareTmpDir(): string
    {
        $tempDir = tempnam(sys_get_temp_dir(), '');

        if (file_exists($tempDir)) {
            unlink($tempDir);
        }

        mkdir($tempDir);

        if (!is_dir($tempDir)) {
            throw new \RuntimeException('Temporary directory was not created.');
        }

        return $tempDir;
    }

    protected function eraseTmpDir(string $path): void
    {
        if (!is_dir($path)) {
            return;
        }

        if (0 !== strpos($path, sys_get_temp_dir())) {
            return;
        }

        $this->eraseDirectoryRecursive($path);
    }

    protected function eraseDirectoryRecursive(string $path): void
    {
        if (!is_dir($path)) {
            unlink($path);

            return;
        }

        $files = glob($path . '/*');
        foreach ($files as $file) {
            $this->eraseDirectoryRecursive($file);
        }

        rmdir($path);
    }
}
