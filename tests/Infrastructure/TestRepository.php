<?php

declare(strict_types=1);

namespace App\Tests\Infrastructure;

use App\Infrastructure\AbstractRepository;

final class TestRepository extends AbstractRepository
{
    protected const FILE_NAME = 'Model.json';

    /**
     * @var string
     */
    public static $filename = self::FILE_NAME;

    public function load(): object
    {
        return $this->loadModel(self::$filename);
    }

    public function save(object $model): void
    {
        $this->saveModel(self::$filename, $model);
    }

    protected function serializeModel(object $model): string
    {
        return json_encode($model);
    }

    protected function unserializeModel(string $data): object
    {
        return $data ? json_decode($data) : new \stdClass();
    }
}
