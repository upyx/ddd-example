<?php

declare(strict_types=1);

namespace App\Tests\Infrastructure;

use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Infrastructure\AbstractRepository
 */
final class AbstractRepositoryTest extends TestCase
{
    use TemporaryDirectory;

    /**
     * @var string
     */
    private static $json = '{"a":"b"}';

    /**
     * @var object
     */
    private static $model;

    /**
     * @var string
     */
    private $tempDir;

    /**
     * @var TestRepository
     */
    private $repository;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::$model = (object) ['a' => 'b'];
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->tempDir = $this->prepareTmpDir();
        $this->repository = new TestRepository($this->tempDir);
    }

    protected function tearDown(): void
    {
        $this->eraseTmpDir($this->tempDir);

        parent::tearDown();
    }

    public function testLoad(): void
    {
        file_put_contents($this->getFilePath(), self::$json);

        $outside = $this->repository->load();

        $this->assertEquals(self::$model, $outside);
    }

    public function testSave(): void
    {
        $this->repository->save(self::$model);
        $json = file_get_contents($this->getFilePath());

        $this->assertSame(self::$json, $json);
    }

    public function testSaveAndLoadWhenNoDataDir(): void
    {
        $this->eraseTmpDir($this->tempDir);

        $this->repository->save(self::$model);
        $model = $this->repository->load();

        $this->assertEquals(self::$model, $model);
    }

    public function testSaveLast(): void
    {
        file_put_contents($this->getFilePath(), self::$json);
        $model = $this->repository->load();
        $this->repository->saveLast();

        $this->assertEquals($model, self::$model);
    }

    private function getFilePath(): string
    {
        return $this->tempDir . DIRECTORY_SEPARATOR . TestRepository::$filename;
    }
}
