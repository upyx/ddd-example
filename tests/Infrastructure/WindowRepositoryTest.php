<?php

declare(strict_types=1);

namespace App\Tests\Infrastructure;

use App\Domain\Model\Window\Window;
use App\Infrastructure\WindowRepository;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Infrastructure\WindowRepository
 */
final class WindowRepositoryTest extends TestCase
{
    use TemporaryDirectory;

    /**
     * @var string
     */
    private static $json = '{"state":"opened","width":2,"height":2}';

    /**
     * @var string
     */
    private static $fileName = 'Window.json';

    /**
     * @var Window
     */
    private static $window;

    /**
     * @var string
     */
    private $tempDir;

    /**
     * @var WindowRepository
     */
    private $repository;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::$window = Window::newOpened(2, 2);
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->tempDir = $this->prepareTmpDir();
        $this->repository = new WindowRepository($this->tempDir);
    }

    protected function tearDown(): void
    {
        $this->eraseTmpDir($this->tempDir);

        parent::tearDown();
    }

    public function testLoadWindow(): void
    {
        file_put_contents($this->getFilePath(), self::$json);

        $window = $this->repository->loadWindow();

        $this->assertEquals(self::$window, $window);
    }

    public function testSaveWindow(): void
    {
        $this->repository->saveWindow(self::$window);
        $json = file_get_contents($this->getFilePath());

        $this->assertSame(self::$json, $json);
    }

    private function getFilePath(): string
    {
        return $this->tempDir . DIRECTORY_SEPARATOR . self::$fileName;
    }
}
